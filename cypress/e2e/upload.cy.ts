describe('In the login page', () => {
  it('login and go to the upload page to upload images', () => {
    cy.visit(`${Cypress.env('api_url')}api/auth/login`)
    cy.get('#username').type('Elysee')
    cy.get('#password').type('my password')
    cy.fixture('upload').then((uploadData: uploadInterface) => {
      cy.contains(uploadData.textLinkLogin).click()
      cy.visit('/envoyer')
      cy.get('.edit-button').click()
      cy.get('#upload-title').clear()
      cy.get('#upload-title').type(uploadData.textTitle)
      cy.contains(uploadData.textButtonTitle).click()
      cy.contains(uploadData.textButtonUpload).click()
    })
    cy.get('.input-file').selectFile(
      [
        '/src/cypress/fixtures/images/image1.jpg',
        '/src/cypress/fixtures/images/image2.jpg',
        '/src/cypress/fixtures/images/image3.jpg'
      ],
      { force: true }
    )
  })
})
interface uploadInterface {
  textLinkLogin: string
  textLinkUpload: string
  textButtonUpload: string
  textTitle: string
  textButtonTitle: string
}
export {}
