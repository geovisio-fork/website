#--------------------------------------------------------------
#- Build image
#-

FROM node:18.16.1-alpine AS build

WORKDIR /opt/geovisio

# Import dependencies files
COPY package.json yarn.lock ./

# Install NodeJS dependencies
RUN yarn install --frozen-lockfile

# Import source code
COPY static ./static
COPY src ./src
COPY *.json *.js *.ts *.html ./

# Replace env variables by placeholder for dynamic change on container start
ENV VITE_INSTANCE_NAME=DOCKER_VITE_INSTANCE_NAME
ENV VITE_RASTER_TILE=DOCKER_VITE_RASTER_TILE
ENV VITE_API_URL=DOCKER_VITE_API_URL
ENV VITE_TILES=DOCKER_VITE_TILES
ENV VITE_MAX_ZOOM=DOCKER_VITE_MAX_ZOOM
ENV VITE_ZOOM=DOCKER_VITE_ZOOM
ENV VITE_CENTER=DOCKER_VITE_CENTER

# Build code
ENV PORT=3000
RUN yarn deploy


#--------------------------------------------------------------
#- Final image
#-

FROM nginx:1-alpine
RUN apk add bash

# Retrieve files from build
COPY --from=build /opt/geovisio/dist /usr/share/nginx/html

# Add Docker scripts and Nginx conf
COPY docker/nginx.conf /etc/nginx/nginx.conf
COPY docker/docker-entrypoint.sh /etc/nginx/docker-entrypoint.sh
RUN chmod +x /etc/nginx/docker-entrypoint.sh

# Define env variables defaults
ENV VITE_INSTANCE_NAME="GeoVisio/Docker"
ENV VITE_API_URL="https://panoramax.openstreetmap.fr"
ENV VITE_TILES="https://tile-vect.openstreetmap.fr/styles/basic/style.json"
ENV VITE_MAX_ZOOM=""
ENV VITE_RASTER_TILE=""
ENV VITE_ZOOM=""
ENV VITE_CENTER=""

# Start Nginx
EXPOSE 3000
ENTRYPOINT ["/etc/nginx/docker-entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]
