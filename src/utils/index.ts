export function createUrlLink(picId: string): string {
  return encodeURIComponent(`${window.location.origin}/#focus=pic&pic=${picId}`)
}
