import axios from 'axios'

async function getIgnTiles(): Promise<object> {
  const { data } = await axios.get(
    'https://wxs.ign.fr/essentiels/static/vectorTiles/styles/PLAN.IGN/attenue.json'
  )
  data.sources.plan_ign.scheme = 'xyz'
  data.sources.plan_ign.attribution = 'Données cartographiques : © IGN'
  const objIndex = data.layers.findIndex(
    (el: { id: string }) => el.id === 'toponyme - parcellaire - adresse'
  )
  data.layers[objIndex].layout = {
    ...data.layers[objIndex].layout,
    'text-field': ['concat', ['get', 'numero'], ['get', 'indice_de_repetition']]
  }
  // Patch tms scheme to xyz to make it compatible for Maplibre GL JS / Mapbox GL JS
  // Patch num_repetition
  return data
}

export { getIgnTiles }
