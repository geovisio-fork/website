import type { ExtendedHtmlEl } from '@/views/interfaces/HomeView'
interface ExtendedDocument extends Document {
  webkitFullscreenElement?: Element
  mozFullScreenElement?: Element
  msFullscreenElement?: Element
  mozCancelFullScreen?: () => void
  webkitExitFullscreen?: () => void
  msExitFullscreen?: () => void
}
export function toggleFullscreen(element: ExtendedHtmlEl) {
  const documentWithExtensions = document as ExtendedDocument
  const isInFullScreen =
    (documentWithExtensions.fullscreenElement &&
      documentWithExtensions.fullscreenElement !== null) ||
    (documentWithExtensions.webkitFullscreenElement &&
      documentWithExtensions.webkitFullscreenElement !== null) ||
    (documentWithExtensions.mozFullScreenElement &&
      documentWithExtensions.mozFullScreenElement !== null) ||
    (documentWithExtensions.msFullscreenElement &&
      documentWithExtensions.msFullscreenElement !== null)

  if (!isInFullScreen) {
    if (element.requestFullscreen) {
      element.requestFullscreen()
    } else if (element.mozRequestFullScreen) {
      element.mozRequestFullScreen()
    } else if (element.webkitRequestFullScreen) {
      element.webkitRequestFullScreen()
    } else if (element.msRequestFullscreen) {
      element.msRequestFullscreen()
    }
  } else {
    if (documentWithExtensions.exitFullscreen) {
      documentWithExtensions.exitFullscreen()
    } else if (documentWithExtensions.webkitExitFullscreen) {
      documentWithExtensions.webkitExitFullscreen()
    } else if (documentWithExtensions.mozCancelFullScreen) {
      documentWithExtensions.mozCancelFullScreen()
    } else if (documentWithExtensions.msExitFullscreen) {
      documentWithExtensions.msExitFullscreen()
    }
  }
}
