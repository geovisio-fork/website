function sortByName(fileList: File[]): File[] {
  return fileList.sort((a: File, b: File) =>
    a.name.localeCompare(b.name, navigator.languages[0] || navigator.language, {
      numeric: true,
      ignorePunctuation: true
    })
  )
}
export { sortByName }
