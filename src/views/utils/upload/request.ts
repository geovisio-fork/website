import axios from 'axios'

interface SequenceCreatedInterface {
  data: {
    created: string
    description: string
    extent: { spatial: object; temporal: object }
    ['geovisio:status']: string
    id: string
    keywords: string[]
    license: string
    links: object[]
    providers: object[]
    stac_extensions: string[]
    stac_version: string
    ['stats:items']: { count: number }
    title: string
    type: string
  }
}

interface PictureCreatedInterface {
  data: {
    assets: object
    bbox: number[]
    collection: string
    geometry: object
    id: string
    links: object[]
    properties: object
    providers: object[]
    stac_extensions: string[]
    stac_version: string
    type: string
  }
}

function createASequence(title: string): Promise<SequenceCreatedInterface> {
  return axios.post('api/collections', { title: title })
}

async function createAPictureToASequence(
  id: string,
  body: FormData
): Promise<PictureCreatedInterface> {
  const { data } = await axios.post(`api/collections/${id}/items`, body)
  return data
}

export { createASequence, createAPictureToASequence }
