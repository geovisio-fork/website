export interface SequenceInterface {
  title: string
  id: string
  pictures: any[]
  picturesOnError: uploadErrorInterface[] | []
  pictureCount: number
  pictureSize: string
}
export interface uploadErrorInterface {
  message: string
  details: { error: string }
  name: string
}
