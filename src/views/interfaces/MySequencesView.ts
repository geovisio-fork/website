export interface SequenceLinkInterface {
  id: string
  href: string
  rel: string
  title: string
  type: string
  created: Date
  extent: { temporal: { interval: [Date[]] }; spatial: { bbox: [number[]] } }
  ['stats:items']: { count: number }
  ['geovisio:status']: string
}
