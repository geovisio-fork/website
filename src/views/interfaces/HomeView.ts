export interface ExtendedHtmlEl extends HTMLElement {
  mozRequestFullScreen?: () => void
  webkitRequestFullScreen?: () => void
  msRequestFullscreen?: () => void
}
