export interface MapInterface {
  startWide?: boolean
  users?: string[]
  maxZoom?: number
  minZoom?: number
  style?: object | string
  zoom?: number
  center?: number[]
  bounds?: number[]
  raster?: {
    type: string
    tiles: string[]
    attribution: string
    tileSize: number
  }
}

export interface ViewerInterface {
  fetchOptions?: {
    credentials: string
  }
  hash?: boolean
  picId?: string
  widgets?: {
    customWidget: HTMLAnchorElement
  }
  map: MapInterface
}
