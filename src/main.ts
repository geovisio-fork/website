import { createApp } from 'vue'
import { createI18n } from 'vue-i18n'
import VueMatomo from 'vue-matomo'
import App from './App.vue'
import router from './router'
import axios from 'axios'
import VueAxios from 'vue-axios'
import { VueDraggableResizable } from 'vue-draggable-resizable-vue3'
import VCalendar from 'v-calendar'
import 'v-calendar/style.css'
import { pinia } from './store'
import fr from './locales/fr.json'
import en from './locales/en.json'
import hu from './locales/hu.json'
import './assets/main.scss'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-icons/font/bootstrap-icons.css'
import 'geovisio/build/index.css'

declare global {
  interface Window {
    _paq: any[]
  }
}

axios.defaults.baseURL = import.meta.env.VITE_API_URL
axios.defaults.withCredentials = true
const matomoHost = import.meta.env.VITE_MATOMO_HOST
const matomoSiteId = import.meta.env.VITE_MATOMO_SITE_ID
const matomoExist = matomoHost && matomoSiteId

const i18n = createI18n({
  locale: navigator.language.split('-')[0],
  fallbackLocale: 'fr',
  warnHtmlMessage: false,
  globalInjection: true,
  legacy: false,
  messages: {
    fr,
    en,
    hu
  }
})

const app = createApp(App)
app.use(pinia)
app.use(i18n)
app.use(router)
app.use(VueAxios, axios)
app.provide('axios', app.config.globalProperties.axios)
app.use(VueDraggableResizable)
app.use(VCalendar)
if (matomoExist) {
  app.use(VueMatomo, {
    host: matomoHost,
    siteId: matomoExist
  })
}
app.mount('#app')
if (matomoExist) {
  window._paq.push(['trackPageView']) // Pour suivre les visites sur vos pages
}
