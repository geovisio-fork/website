import { it, describe, expect, vi } from 'vitest'
import { shallowMount, mount } from '@vue/test-utils'
import UploadPicturesView from '../../../views/UploadPicturesView.vue'
import i18n from '../config'
import InputUpload from '../../../components/InputUpload.vue'
import * as createAPictureToASequence from '@/views/utils/upload/request'
import * as createASequence from '@/views/utils/upload/request'
import { formatDate } from '../../../utils/dates'
import * as sortByName from '../../../views/utils/upload/index'

describe('Template', () => {
  it('should render the view with the input upload and the good wordings', () => {
    const wrapper = shallowMount(UploadPicturesView, {
      global: {
        plugins: [i18n],
        mocks: {
          $t: (msg) => msg
        }
      }
    })
    expect(wrapper.html()).contains('pages.upload.title')
    expect(wrapper.html()).contains('pages.upload.description')
    expect(wrapper.html()).contains('<input-upload-stub')
    expect(wrapper.html()).contains('pages.upload.input_label')
  })
  describe('trigger addPictures', () => {
    describe('submit uploadPicture', () => {
      it('should trigger to uploadPictures', async () => {
        const wrapper = mount(UploadPicturesView, {
          global: {
            plugins: [i18n],
            mocks: {
              $t: (msg) => msg
            },
            components: {
              InputUpload
            }
          }
        })
        const sortByNameMock = vi.spyOn(sortByName, 'sortByName')
        sortByNameMock.mockReturnValue([{}, {}])
        const wrapperInputUpload = wrapper.findComponent(InputUpload)
        await wrapperInputUpload.trigger('trigger')
        await wrapperInputUpload.vm.$emit('trigger', [{}, {}])
        expect(wrapper.html()).contains('class="wrapper-uploading"')
        expect(wrapper.html()).contains('pages.upload.uploading_process')
        expect(wrapper.html()).contains('class="uploading-img"')
        expect(wrapper.html()).contains('class="loader-percentage"')
      })
    })
    describe('one sequence has been imported', () => {
      it('should render a sequence with a list of uploaded pictures', async () => {
        const wrapper = mount(UploadPicturesView, {
          global: {
            stubs: {
              modal: {
                template: '<div></div>'
              }
            },
            plugins: [i18n],
            mocks: {
              $t: (msg) => msg
            }
          }
        })
        const picture = {
          lastModified: 1599133968750,
          name: '100MSDCF_DSC02790.JPG',
          size: 2345,
          type: 'image/jpeg'
        }
        const spyASequence = vi.spyOn(createASequence, 'createASequence')
        const spyPicture = vi.spyOn(
          createAPictureToASequence,
          'createAPictureToASequence'
        )
        const sortByNameMock = vi.spyOn(sortByName, 'sortByName')
        sortByNameMock.mockReturnValue([picture])
        const sequenceId = 'my-id'
        spyASequence.mockReturnValue({ data: { id: sequenceId } })
        spyPicture.mockReturnValue({ data: {} })
        const sequenceTitle = `Séquence du ${formatDate(
          new Date(),
          'Do MMMM YYYY, HH:mm:ss'
        )}`
        const body = new FormData()
        body.append('position', '1')
        body.append('picture', picture)
        const wrapperInputUpload = wrapper.findComponent(InputUpload)
        await wrapperInputUpload.trigger('trigger')
        await wrapperInputUpload.vm.$emit('trigger', [picture])

        expect(spyASequence).toHaveBeenCalledWith(sequenceTitle)
        expect(spyPicture).toHaveBeenCalledWith(sequenceId, body)
        expect(wrapper.html()).contains('class="loader-percentage">0%')
      })
    })
  })
})
