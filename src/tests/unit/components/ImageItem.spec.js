import { it, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import ImageItem from '../../../components/ImageItem.vue'

describe('Template', () => {
  describe('Props', () => {
    it('should have default props', () => {
      const wrapper = shallowMount(ImageItem, {
        global: {
          mocks: {
            $t: (msg) => msg
          }
        }
      })

      expect(wrapper.vm.created).toBe(null)
      expect(wrapper.vm.href).toBe(null)
      expect(wrapper.vm.hrefHd).toBe(null)
      expect(wrapper.vm.selected).toBe(false)
      expect(wrapper.vm.selectedOnMap).toBe(false)
      expect(wrapper.vm.status).toBe('')
    })
  })
  describe('When the component have default props filled', () => {
    it('should render the component with a href, a hrefHd and a date', () => {
      const wrapper = shallowMount(ImageItem, {
        global: {
          mocks: {
            $t: (msg) => msg
          }
        },
        props: {
          created: '10 mars 2023',
          href: 'my-url',
          hrefHd: 'my-url-hd',
          status: 'ready'
        }
      })
      expect(wrapper.html()).contains('<img')
      expect(wrapper.html()).contains('src="my-url"')
      expect(wrapper.html()).contains('<link-stub')
      expect(wrapper.html()).contains('10 mars 2023')
    })
  })
  describe('When the component is selected', () => {
    it('should render the component with the selected class', () => {
      const wrapper = shallowMount(ImageItem, {
        global: {
          mocks: {
            $t: (msg) => msg
          }
        },
        props: {
          selected: true
        }
      })
      expect(wrapper.html()).contains('class="selected button-image-item"')
    })
  })
  describe('When the component is ready', () => {
    it('should render the component with the status class', () => {
      const wrapper = shallowMount(ImageItem, {
        global: {
          mocks: {
            $t: (msg) => msg
          }
        },
        props: {
          href: 'my-url',
          status: 'ready'
        }
      })

      expect(wrapper.html()).contains('src="my-url"')
      expect(wrapper.html()).contains('class="ready"')
    })
  })
  describe('When the component is waiting-for-process', () => {
    it('should render the component with waiting-for-process classes and elements', () => {
      const wrapper = shallowMount(ImageItem, {
        global: {
          mocks: {
            $t: (msg) => msg
          }
        },
        props: {
          status: 'waiting-for-process',
          href: 'my-url'
        }
      })
      expect(wrapper.html()).contains('class="waiting-for-process"')
      expect(wrapper.html()).contains('pages.sequence.waiting_process')
      expect(wrapper.html()).contains('class="bi bi-card-image icon-waiting"')
    })
  })
  describe('When the component is selected on the map but not selected', () => {
    it('should render the component with the map pointer', () => {
      const wrapper = shallowMount(ImageItem, {
        global: {
          mocks: {
            $t: (msg) => msg
          }
        },
        props: {
          selectedOnMap: true,
          selected: false
        }
      })
      expect(wrapper.html()).contains('class="icon-img pointer-map"')
    })
  })
  describe('When the component is selected but not selected on the map', () => {
    it('should render the component with the check icon', () => {
      const wrapper = shallowMount(ImageItem, {
        global: {
          mocks: {
            $t: (msg) => msg
          }
        },
        props: {
          selectedOnMap: false,
          selected: true
        }
      })
      expect(wrapper.html()).contains('class="bi bi-check-lg"')
      expect(wrapper.html()).contains('class="icon-img button-check"')
    })
  })
  describe('When the component is selected and selected on the map', () => {
    it('should render the component with the check icon', () => {
      const wrapper = shallowMount(ImageItem, {
        global: {
          mocks: {
            $t: (msg) => msg
          }
        },
        props: {
          selectedOnMap: true,
          selected: true
        }
      })
      expect(wrapper.html()).contains('class="icon-img button-check-pointer"')
      expect(wrapper.html()).contains('class="bi bi-check-lg"')
    })
  })
  describe('When the button is trigger', () => {
    it('should emit', () => {
      const wrapper = shallowMount(ImageItem, {
        global: {
          mocks: {
            $t: (msg) => msg
          }
        }
      })
      wrapper.vm.$emit('trigger')
      expect(wrapper.emitted().trigger).toBeTruthy()
    })
  })
})
