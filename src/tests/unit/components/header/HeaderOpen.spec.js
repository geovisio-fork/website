import { vi, it, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import HeaderOpen from '../../../../components/header/HeaderOpen.vue'
import i18n from '../../config'
import { createRouter, createWebHistory } from 'vue-router/dist/vue-router'
vi.mock('vue-router')

const router = createRouter({
  history: createWebHistory(),
  routes: [{ path: '/', component: { template: '<div></div>' } }]
})
describe('Template', () => {
  describe('Props', () => {
    it('should have default props', () => {
      const wrapper = shallowMount(HeaderOpen, {
        global: {
          plugins: [i18n, router]
        }
      })
      expect(wrapper.vm.menuIsClosed).toBe(true)
      expect(wrapper.vm.userProfileUrlLength).toBe(0)
    })
    describe('When all the props are filled', () => {
      describe('When the user is in desktop', () => {
        it('should render the desktop entries', () => {
          const wrapper = shallowMount(HeaderOpen, {
            global: {
              plugins: [i18n, router]
            },
            props: {
              menuIsClosed: false,
              userProfileUrlLength: 5
            }
          })
          expect(wrapper.html()).contains('class="logged-link desktop"')
          expect(wrapper.html()).contains('text="Pourquoi contribuer ?"')
        })
      })
      describe('When the user is in mobile', () => {
        it('should render the responsive entries', () => {
          const wrapper = shallowMount(HeaderOpen, {
            global: {
              plugins: [i18n, router]
            },
            props: {
              menuIsClosed: false,
              userProfileUrlLength: 5
            }
          })
          expect(wrapper.html()).contains('class="logged-link responsive"')
          expect(wrapper.html()).contains('text="Mes photos"')
          expect(wrapper.html()).contains('class="separator responsive"')
        })
      })
      it('should render all the commons entries', () => {
        import.meta.env.VITE_API_URL = 'api-url/'
        const wrapper = shallowMount(HeaderOpen, {
          global: {
            plugins: [i18n, router]
          },
          props: {
            menuIsClosed: false,
            userProfileUrlLength: 5
          }
        })
        expect(wrapper.html()).contains('text="Pourquoi contribuer ?"')
        expect(wrapper.html()).contains('text="+  Partager des photos"')
        expect(wrapper.html()).contains('text="Mes informations"')
        expect(wrapper.html()).contains('text="Mes paramètres"')
        expect(wrapper.html()).contains('text="Déconnexion"')
        expect(wrapper.html()).contains('api/auth/logout')
      })
    })
  })
})
