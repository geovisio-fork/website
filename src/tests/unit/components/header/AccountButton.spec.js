import { it, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import AccountButton from '../../../../components/header/AccountButton.vue'
import i18n from '../../config'

describe('Template', () => {
  describe('Props', () => {
    it('should have default props', () => {
      const wrapper = shallowMount(AccountButton, {
        global: {
          plugins: [i18n]
        }
      })
      expect(wrapper.vm.menuIsClosed).toBe(true)
      expect(wrapper.vm.isLogged).toBe(false)
      expect(wrapper.vm.userName).toBe(null)
      expect(wrapper.vm.ariaLabel).toBe(null)
    })
    it('should have all props filled', () => {
      const wrapper = shallowMount(AccountButton, {
        global: {
          plugins: [i18n]
        },
        props: {
          menuIsClosed: false,
          isLogged: true,
          userName: 'User',
          ariaLabel: 'label'
        }
      })
      expect(wrapper.html()).contains('aria-label="label"')
      expect(wrapper.html()).contains('class="wrapper-name"')
      expect(wrapper.html()).contains('User')
      expect(wrapper.html()).contains('Mon compte')
      expect(wrapper.html()).contains('class="arrow-img arrow-up"')
    })
  })
})
