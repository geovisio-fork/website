import { test, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import Footer from '../../../components/Footer.vue'
import Link from '../../../components/Link.vue'
import i18n from '../config'
describe('Template', () => {
  test('should have the links without the ay11 link', () => {
    const wrapper = shallowMount(Footer, {
      global: {
        stubs: { Link },
        plugins: [i18n]
      }
    })
    expect(wrapper.html()).contains('href="https://panoramax.fr/"')
    expect(wrapper.html()).contains('logo.jpeg')
    expect(wrapper.html()).contains('Découvrir Panoramax')

    expect(wrapper.html()).contains('href="https://gitlab.com/geovisio"')
    expect(wrapper.html()).contains('gitlab-logo.svg')
    expect(wrapper.html()).contains('Voir le code')
  })
  test('should have the ay11 link', () => {
    Object.create(window)
    const url = 'http://test.ign.fr'
    Object.defineProperty(window, 'location', {
      value: {
        href: url
      },
      writable: true // possibility to override
    })
    const wrapper = shallowMount(Footer, {
      global: {
        stubs: { Link },
        plugins: [i18n]
      }
    })
    expect(wrapper.html()).contains('Accessibilité : non conforme')
  })
})
