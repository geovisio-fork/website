import { test, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import Button from '../../../components/Button.vue'
import i18n from '../config'
describe('Template', () => {
  describe('Props', () => {
    test('should have default props', () => {
      const wrapper = shallowMount(Button, {
        global: {
          plugins: [i18n]
        }
      })
      expect(wrapper.vm.icon).toBe(null)
      expect(wrapper.vm.disabled).toBe(false)
      expect(wrapper.vm.isLoading).toBe(false)
      expect(wrapper.vm.text).toBe('')
      expect(wrapper.vm.tooltip).toBe('')
      expect(wrapper.vm.look).toBe('')
      expect(wrapper.vm.type).toBe('button')
    })
  })
  describe('When the component is disabled', () => {
    test('should render the button with disabled class', () => {
      const wrapper = shallowMount(Button, {
        global: {
          plugins: [i18n]
        },
        props: {
          disabled: true
        }
      })
      expect(wrapper.html()).contains('class="default disabled"')
    })
  })
  describe('When the component is loading', () => {
    test('should render the button with disabled class', () => {
      const wrapper = shallowMount(Button, {
        global: {
          plugins: [i18n]
        },
        props: {
          isLoading: true
        }
      })
      expect(wrapper.html()).contains('class="default disabled"')
    })
  })
  describe('When the component have an icon', () => {
    test('should render the button the icon displayed', () => {
      const wrapper = shallowMount(Button, {
        global: {
          plugins: [i18n]
        },
        props: {
          icon: 'my-icon'
        }
      })
      expect(wrapper.html()).contains('<i')
      expect(wrapper.html()).contains('class="my-icon icon"')
    })
  })
  describe('When the component have a type submit', () => {
    test('should render the type to submit', () => {
      const wrapper = shallowMount(Button, {
        global: {
          plugins: [i18n]
        },
        props: {
          type: 'submit'
        }
      })
      expect(wrapper.html()).contains('type="submit"')
    })
  })
  describe('When the component have tooltip', () => {
    test('should render the button with a tooltip', () => {
      const wrapper = shallowMount(Button, {
        global: {
          plugins: [i18n]
        },
        props: {
          tooltip: 'my tooltip'
        }
      })
      expect(wrapper.html()).contains(
        'class="tooltip-button">my tooltip</span>'
      )
    })
  })
  describe('When the component have a text', () => {
    test('should render the button with a text', () => {
      const wrapper = shallowMount(Button, {
        global: {
          plugins: [i18n]
        },
        props: {
          text: 'My text'
        }
      })
      expect(wrapper.html()).contains('class="text">My text</span>')
    })
  })
  describe('When the component have specific look', () => {
    test('should render the button with a specific class', () => {
      const wrapper = shallowMount(Button, {
        global: {
          plugins: [i18n]
        },
        props: {
          look: 'my--look'
        }
      })
      expect(wrapper.html()).contains('class="my--look default"')
    })
  })
  describe('When the button is trigger', () => {
    test('should emit', async () => {
      const wrapper = shallowMount(Button, {
        global: {
          plugins: [i18n]
        }
      })
      await wrapper.trigger('click')
      expect(wrapper.emitted()).toHaveProperty('trigger')
    })
  })
})
