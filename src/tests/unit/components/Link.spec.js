import { test, describe, vi, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import Link from '../../../components/Link.vue'
import { createI18n } from 'vue-i18n'
import { createRouter, createWebHistory } from 'vue-router'
import fr from '../../../locales/fr.json'
vi.mock('vue-i18n')

const i18n = createI18n({
  locale: 'fr',
  fallbackLocale: 'fr',
  globalInjection: true,
  legacy: false,
  messages: {
    fr
  }
})
const router = createRouter({
  history: createWebHistory(),
  routes: [{ path: '/', component: { template: '<div></div>' } }]
})
const stubs = {
  'router-link': {
    template: '<router-link></router-link>'
  }
}
describe('Template', () => {
  describe('Props', () => {
    test('should have default props', () => {
      const wrapper = shallowMount(Link, {
        global: {
          plugins: [i18n, router]
        }
      })

      expect(wrapper.vm.text).toBe(null)
      expect(wrapper.vm.route).toStrictEqual({})
      expect(wrapper.vm.pathExternal).toBe('')
      expect(wrapper.vm.look).toBe('')
      expect(wrapper.vm.type).toBe(null)
      expect(wrapper.vm.alt).toBe('')
      expect(wrapper.vm.icon).toBe(null)
      expect(wrapper.vm.target).toBe(null)
      expect(wrapper.vm.image).toBe(null)
      expect(wrapper.vm.disabled).toBe(false)
    })
  })
  describe('When the component is an external link', () => {
    test('should render the component as a <a>', () => {
      const wrapper = shallowMount(Link, {
        global: {
          stubs,
          plugins: [i18n]
        },
        props: {
          type: 'external'
        }
      })
      expect(wrapper.html()).contains('<a')
    })
    test('should render an icon inside the link', () => {
      const wrapper = shallowMount(Link, {
        global: {
          stubs,
          plugins: [i18n]
        },
        props: {
          type: 'external',
          icon: 'my-icon'
        }
      })
      expect(wrapper.html()).contains('my-icon')
    })
    test('should render the text inside the link', () => {
      const wrapper = shallowMount(Link, {
        global: {
          stubs,
          plugins: [i18n]
        },
        props: {
          type: 'external',
          text: 'my-text'
        }
      })
      expect(wrapper.html()).contains('my-text')
    })
    test('should render an image inside the link', () => {
      const wrapper = shallowMount(Link, {
        global: {
          stubs,
          plugins: [i18n]
        },
        props: {
          type: 'external',
          image: {
            url: 'my-url.png',
            alt: 'my-alt'
          }
        }
      })
      expect(wrapper.html()).contains('<img')
      expect(wrapper.html()).contains('my-url.png')
      expect(wrapper.html()).contains('my-alt')
    })
    test('should render the text inside the link', () => {
      const wrapper = shallowMount(Link, {
        global: {
          stubs,
          plugins: [i18n]
        },
        props: {
          type: 'external',
          disabled: true
        }
      })
      expect(wrapper.html()).contains('disabled')
    })
  })

  describe('When the component is an internal link', () => {
    test('should render the component as an internal link', () => {
      const wrapper = shallowMount(Link, {
        global: {
          plugins: [i18n, router]
        }
      })
      expect(wrapper.html()).contains('<router-link')
    })
    test('should render an icon inside the link', () => {
      const wrapper = shallowMount(Link, {
        global: {
          plugins: [i18n]
        },
        props: {
          type: 'internal',
          icon: 'my-icon'
        }
      })
      expect(wrapper.html()).contains('my-icon')
    })
    test('should render the text inside the link', () => {
      const wrapper = shallowMount(Link, {
        global: {
          plugins: [i18n, router]
        },
        props: {
          text: 'my-text'
        }
      })
      expect(wrapper.html()).contains('my-text')
    })
    test('should render the disabled attribute', () => {
      const wrapper = shallowMount(Link, {
        global: {
          plugins: [i18n, router]
        },
        props: {
          disabled: true
        }
      })
      expect(wrapper.html()).contains('disabled')
    })
  })
})
