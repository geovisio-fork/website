import { test, describe, expect, vi } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import EditText from '../../../components/EditText.vue'
import Button from '../../../components/Button.vue'
import i18n from '../config'
describe('Template', () => {
  describe('Props', () => {
    test('should have default props', () => {
      const wrapper = shallowMount(EditText, {
        global: {
          plugins: [i18n]
        }
      })
      expect(wrapper.vm.defaultText).toBe(null)
    })
  })
  describe('When the props are filled', () => {
    test('should render the component with the defaultText', () => {
      const wrapper = shallowMount(EditText, {
        global: {
          plugins: [i18n]
        },
        props: {
          defaultText: 'My default text'
        }
      })
      expect(wrapper.html()).contains('class="title">My default text</span>')
      expect(wrapper.html()).contains('icon="bi bi-pen"')
    })
  })
  describe('When the component is in editable mode', () => {
    test('should render the component with the element to edit the title', async () => {
      const wrapper = shallowMount(EditText, {
        global: {
          plugins: [i18n]
        }
      })
      wrapper.vm.isEditTitle = true
      wrapper.vm.titleToEdit = 'title to edit'
      await wrapper.vm.$nextTick()

      expect(wrapper.html()).contains('text="title to edit"')
      expect(wrapper.html()).contains('icon="bi bi-x"')
      expect(wrapper.html()).contains('text="Valider"')
    })
    test('should valid the name and emit', async () => {
      const wrapper = shallowMount(EditText, {
        global: {
          plugins: [i18n],
          components: {
            Button
          }
        }
      })
      wrapper.vm.isEditTitle = true
      wrapper.vm.titleToEdit = 'title to edit'
      await wrapper.vm.$nextTick()

      const validationButton = wrapper.findComponent('#valid-button')
      await validationButton.vm.$emit('trigger')

      expect(wrapper.emitted()).toHaveProperty('triggerNewText')
      expect(wrapper.emitted().triggerNewText[0][0]).toEqual(
        wrapper.vm.titleToEdit
      )
    })
    test('should close the edit mode', async () => {
      const wrapper = shallowMount(EditText, {
        global: {
          plugins: [i18n],
          components: {
            Button
          }
        }
      })
      wrapper.vm.isEditTitle = true
      wrapper.vm.titleToEdit = 'title to edit'
      await wrapper.vm.$nextTick()

      const closeButton = wrapper.findComponent('#close-button')
      await closeButton.vm.$emit('trigger')
      await wrapper.vm.$nextTick()

      expect(wrapper.vm.isEditTitle).toEqual(false)
      expect(wrapper.vm.titleToEdit).toEqual(null)
    })
  })
})
