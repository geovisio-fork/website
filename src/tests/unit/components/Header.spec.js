import { vi, it, beforeEach, describe, expect } from 'vitest'
import { shallowMount, mount } from '@vue/test-utils'
import { createI18n } from 'vue-i18n'
import { createRouter, createWebHistory } from 'vue-router'
import { useCookies } from 'vue3-cookies'
import fr from '../../../locales/fr.json'
import Header from '../../../components/Header.vue'
vi.mock('vue-router')
vi.mock('vue3-cookies', () => {
  const mockCookies = {
    get: vi.fn()
  }
  return {
    useCookies: () => ({
      cookies: mockCookies
    })
  }
})
const i18n = createI18n({
  locale: 'fr',
  fallbackLocale: 'fr',
  globalInjection: true,
  legacy: false,
  messages: {
    fr
  }
})

const router = createRouter({
  history: createWebHistory(),
  routes: [{ path: '/', component: { template: '<div></div>' } }]
})

describe('Template', () => {
  describe('When the user is not logged', () => {
    describe('When the user is in desktop', () => {
      it('should render the component with the desktop entries', async () => {
        import.meta.env.VITE_API_URL = 'api-url/'
        const wrapper = mount(Header, {
          global: {
            plugins: [i18n, router],
            mocks: {
              $t: (msg) => msg
            }
          }
        })
        expect(wrapper.html()).contains('class="wrapper-logo desktop"')
        expect(wrapper.html()).contains('logo.jpeg"')
        expect(wrapper.html()).contains('class="wrapper-instance"')
        expect(wrapper.html()).contains('general.header.title')
        expect(wrapper.html()).contains(
          'class="desktop" data-test="button-login-desktop"'
        )
        expect(wrapper.html()).contains('api-url/api/auth/login')
        expect(wrapper.html()).contains('general.header.login_text')
        expect(wrapper.html()).contains('general.header.register_text')
      })
    })
    describe('When the user is in mobile', () => {
      it('should render the component with the responsive entries', async () => {
        import.meta.env.VITE_API_URL = 'api-url/'
        const wrapper = mount(Header, {
          global: {
            plugins: [i18n, router],
            mocks: {
              $t: (msg) => msg
            }
          }
        })
        expect(wrapper.html()).contains('class="responsive entry-instance"')
        expect(wrapper.html()).contains('general.header.title')
        expect(wrapper.html()).contains('class="wrapper-logo responsive"')
        expect(wrapper.html()).contains('logo.jpeg')
        expect(wrapper.html()).contains(
          'class="responsive" data-test="button-login-responsive"'
        )
        expect(wrapper.html()).contains('api-url/api/auth/login')
        expect(wrapper.html()).contains('general.header.login_text')
        expect(wrapper.html()).contains('general.header.register_text')
      })
    })
    describe('When the user is in desktop OR in mobile', () => {
      it('should render the component with the commons entries', async () => {
        import.meta.env.VITE_API_URL = 'api-url/'
        const wrapper = mount(Header, {
          global: {
            plugins: [i18n, router],
            mocks: {
              $t: (msg) => msg
            }
          }
        })
        expect(wrapper.html()).contains('general.header.contribute_text')
      })
    })
  })
  describe('When the user is logged', () => {
    it('should render the component with good wording keys', async () => {
      vi.spyOn(useCookies().cookies, 'get').mockReturnValue(
        '.eJw9y0EKgzAQQNG7zLoDJpmYxMuUySRDra0pooUi3r3SRZcf_tuBRdo2rzDsMBYYgFxRytljkeyQrK0YVT16m6OhUlIihgvM_Kznfa88n9V4W2_XnzcuiqgmDBQMUtYec00WO3XqAndd7OUvXkt7j6Uup5vqRx6NJziOL8SPLNU.ZVy19Q.4DkVxu-LSF11uREkn6YIwHbn_0U'
      )

      import.meta.env.VITE_API_URL = 'api-url/'
      const wrapper = shallowMount(Header, {
        props: {
          authEnabled: true,
          userProfileUrl: 'profil'
        },
        global: {
          plugins: [i18n, router],
          mocks: {
            $t: (msg) => msg
          }
        }
      })
      expect(wrapper.html()).contains('<header-open')
      expect(wrapper.html()).contains('general.header.sequences_text')
      expect(wrapper.html()).contains('general.header.upload_text')
      expect(wrapper.html()).contains('data-test="link-logged-upload"')
      expect(wrapper.html()).contains('<account-button')
      expect(wrapper.html()).contains('username="J"')
    })
  })
})

describe('Methods', () => {
  describe('toggleMenu', () => {
    let wrapper
    beforeEach(async () => {
      const VueRouter = await import('vue-router')
      VueRouter.useRoute.mockReturnValueOnce({
        path: 'my-path'
      })
    })
    it('Menu should be closed by default', () => {
      wrapper = shallowMount(Header, {
        global: {
          plugins: [i18n, router],
          mocks: {
            $t: (msg) => msg
          }
        }
      })

      expect(wrapper.vm.menuIsClosed).toBe(true)
    })
  })
})
