import { it, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import UploadLoader from '../../../../components/upload/UploadLoader.vue'
import i18n from '../../config'

describe('Template', () => {
  describe('Props', () => {
    it('should have default props', () => {
      const wrapper = shallowMount(UploadLoader, {
        global: {
          plugins: [i18n]
        }
      })
      expect(wrapper.vm.loadPercentage).toBe('0%')
      expect(wrapper.vm.isLoaded).toBe(false)
      expect(wrapper.vm.picturesCount).toBe(null)
      expect(wrapper.vm.uploadedSequence).toStrictEqual(null)
    })
    describe('When some props are filled', () => {
      describe('When the loader is loading', () => {
        it('should have a loading state', () => {
          const wrapper = shallowMount(UploadLoader, {
            global: {
              plugins: [i18n]
            },
            props: {
              loadPercentage: '50%',
              picturesCount: 1,
              uploadedSequence: {
                title: 'Séquence du 5 octobre 2023, 11:25:51',
                id: '3e8ee06aa190bea',
                pictures: [{}, {}],
                picturesOnError: []
              }
            }
          })
          expect(wrapper.html()).contains('text="Accéder à cette séquence"')
          expect(wrapper.html()).contains('disabled="true"')
          expect(wrapper.html()).contains(
            'Une fois chargée, la séquence sera en traitement et accessible sur Panoramax dans les prochaines minutes.'
          )
          expect(wrapper.html()).contains('class="loader-percentage">50%')
          expect(wrapper.html()).contains('<loader')
          expect(wrapper.html()).contains('look="md"')
          expect(wrapper.html()).contains('class="picture-length">2')
        })
      })
      describe('When the loader is loaded', () => {
        it('should have a loading state', () => {
          const wrapper = shallowMount(UploadLoader, {
            global: {
              plugins: [i18n]
            },
            props: {
              loadPercentage: '50%',
              picturesCount: 1,
              uploadedSequence: {
                title: 'Séquence du 5 octobre 2023, 11:25:51',
                id: '3e8ee06caaa190bea',
                pictures: [{}, {}],
                picturesOnError: []
              },
              isLoaded: true
            }
          })
          expect(wrapper.html()).contains('text="Accéder à cette séquence"')
          expect(wrapper.html()).contains('disabled="false"')
          expect(wrapper.html()).contains(
            'La séquence est chargée et est en cours de traitement. Elle sera accessible sur Panoramax dans quelques minutes.'
          )
          expect(wrapper.html()).contains(
            'Le chargement de la séquence est terminé'
          )
        })
      })
    })
  })
})
