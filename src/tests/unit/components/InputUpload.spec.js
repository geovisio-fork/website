import { test, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import InputUpload from '../../../components/InputUpload.vue'
import i18n from '../config'
describe('Template', () => {
  describe('Props', () => {
    test('should have default props', () => {
      const wrapper = shallowMount(InputUpload, {
        global: {
          plugins: [i18n]
        }
      })
      expect(wrapper.vm.text).toBe(null)
      expect(wrapper.vm.textPictureType).toBe(null)
      expect(wrapper.vm.textSecondPart).toBe(null)
      expect(wrapper.vm.accept).toBe('')
    })
    test('should have all the props filled', () => {
      const wrapper = shallowMount(InputUpload, {
        global: {
          plugins: [i18n]
        },
        props: {
          text: 'my text',
          textPictureType: 'my textPictureType',
          textSecondPart: 'my textSecondPart',
          accept: 'accept'
        }
      })
      expect(wrapper.html()).contains('accept="accept"')
      expect(wrapper.html()).contains('my text')
      expect(wrapper.html()).contains('my textSecondPart')
      expect(wrapper.html()).contains('my textPictureType')
    })
  })
  describe('When the input is dragover', () => {
    test('should set the component dragging state', async () => {
      const wrapper = shallowMount(InputUpload, {
        global: {
          plugins: [i18n]
        }
      })
      await wrapper.find('label').trigger('dragover')
      expect(wrapper.vm.isDragging).toBe(true)
      expect(wrapper.html()).contains('class="file-upload dragging"')
    })
  })
  describe('When the input is dragleave', () => {
    test('should set the component dragging state', async () => {
      const wrapper = shallowMount(InputUpload, {
        global: {
          plugins: [i18n]
        }
      })
      await wrapper.find('label').trigger('dragover')
      await wrapper.find('label').trigger('dragleave')

      expect(wrapper.vm.isDragging).toBe(false)
      expect(wrapper.html()).contains('class="file-upload"')
    })
  })
  describe('When the input is dropped', () => {
    test('should emit with value', async () => {
      const wrapper = shallowMount(InputUpload, {
        global: {
          plugins: [i18n]
        }
      })
      const dataTransfer = {
        files: [{ type: 'image/jpeg' }]
      }
      const label = await wrapper.find('label')
      await label.trigger('drop', { dataTransfer })

      expect(wrapper.emitted()).toHaveProperty('trigger')
      expect(wrapper.emitted().trigger[0][0]).toEqual(dataTransfer.files)
    })
  })
  // TODO TEST -> When the input is changed should emit with value
})
