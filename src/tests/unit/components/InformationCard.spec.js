import { test, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import InformationCard from '../../../components/InformationCard.vue'
import i18n from '../config'
describe('Template', () => {
  describe('Props', () => {
    test('should have default props', () => {
      const wrapper = shallowMount(InformationCard, {
        global: {
          plugins: [i18n]
        }
      })
      expect(wrapper.vm.text).toBe(null)
      expect(wrapper.vm.title).toBe(null)
      expect(wrapper.vm.look).toBe('')
    })
    test('should have all the props filled', () => {
      const wrapper = shallowMount(InformationCard, {
        global: {
          plugins: [i18n]
        },
        props: {
          text: 'my text',
          title: 'my title',
          look: 'my-look'
        }
      })
      expect(wrapper.html()).contains('my title</h3>')
      expect(wrapper.html()).contains('my text</p>')
      expect(wrapper.html()).contains('class="information-block my-look"')
    })
  })
})
