import { test, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import Card from '../../../../components/share-pictures/Card.vue'
import i18n from '../../config'
describe('Template', () => {
  describe('Props', () => {
    test('should have default props', () => {
      const wrapper = shallowMount(Card, {
        global: {
          plugins: [i18n]
        }
      })
      expect(wrapper.vm.title).toBe(null)
      expect(wrapper.vm.description).toBe(null)
      expect(wrapper.vm.imgSrc).toBe(null)
      expect(wrapper.vm.imgAlt).toBe(null)
    })
    test('should have all the props filled', () => {
      const wrapper = shallowMount(Card, {
        global: {
          plugins: [i18n]
        },
        props: {
          title: 'my title',
          description: 'my description',
          imgSrc: 'my-imgSrc.jpg',
          imgAlt: 'my imgAlt'
        }
      })
      expect(wrapper.html()).contains('my title</h2>')
      expect(wrapper.html()).contains('my description</p>')
      expect(wrapper.html()).contains('/my-imgSrc.jpg')
      expect(wrapper.html()).contains('alt="my imgAlt"')
    })
  })
})
