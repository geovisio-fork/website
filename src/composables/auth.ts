import axios from 'axios'
import { onMounted, ref } from 'vue'
import type { AuthConfigInterface } from './interfaces/Auth'

export default function authConfig() {
  const authConf = ref<AuthConfigInterface>()

  async function getConfig(): Promise<AuthConfigInterface> {
    const { data } = await axios.get('api/configuration', {
      withCredentials: false
    })
    return data
  }

  onMounted(async () => (authConf.value = await getConfig()))
  return { authConf }
}
