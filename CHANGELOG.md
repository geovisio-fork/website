# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

Before _0.1.0_, website development was on rolling release, meaning there are no version tags.

## [2.4.1] - 2024-02-01

### Fixed

- Fix geovisio version yarn.lock

## [2.4.0] - 2024-01-31

### Added

- Possibility to edit a sequence title in the sequence page

### Changed

- GeoVisio web viewer updated to [2.4.0](https://gitlab.com/geovisio/web-viewer/-/compare/2.3.1...2.4.0) to manage sequence by user

### Fixed

- Fix filter reset button to include bbox filter
- Fix fullscreen button added by the widget viewer
- Some UI and UX fixes before user tests

## [2.3.1] - 2024-01-29

### Added

- Add the possibility to fullscreen the viewer in the homepage : https://gitlab.com/geovisio/website/-/issues/60
- In the sequence list page add a filter by bbox in the map : https://gitlab.com/geovisio/website/-/issues/61
- In the sequence list page add a filter by date in the list : https://gitlab.com/geovisio/website/-/issues/57
- Add a cancel button to when a sequence is uploading to cancel the upload

### Changed

- GeoVisio web viewer updated to [2.3.1](https://gitlab.com/geovisio/web-viewer/-/compare/2.3.0...2.3.1)

### Fixed

- Some UI and UX fixes before user tests

## [2.3.0] - 2023-12-06

### Added

- Add the possibility to an user to select a sequence in the list using the map : https://gitlab.com/geovisio/website/-/merge_requests/100
- For a selected sequence in the list, if the sequence is not displayed in the map, fly to the sequence on the map : https://gitlab.com/geovisio/website/-/merge_requests/108
- Add the pagination to the sequence with sort with API routes : https://gitlab.com/geovisio/website/-/merge_requests/107
- Add Hungarian translation : https://gitlab.com/geovisio/website/-/merge_requests/105
- Add the possibility to hide/delete a sequence in the sequence list : https://gitlab.com/geovisio/website/-/merge_requests/101
- Add the possibility for the user to change the title of the sequence before upload the pictures : https://gitlab.com/geovisio/website/-/merge_requests/101
- Add the possibility to resize the sequence page by dragging the blocs : https://gitlab.com/geovisio/website/-/merge_requests/101

### Changed

- GeoVisio web viewer updated to [2.3.0](https://gitlab.com/geovisio/web-viewer/-/compare/2.2.1...2.3.0)

### Fixed

- Nginx server in Docker container was not recognizing routes other than `/` on first loading.
- Fix the cookie bug by decoding flask cookie : https://gitlab.com/geovisio/website/-/merge_requests/102

## [2.2.3] - 2023-11-03

### Added

- Add translation based on the browser language (only trad for FR and EN for now)

### Changed

- Page My Sequences, add the possibility to select a sequence in the list with the map :

  - the user can only see his sequences on the list
  - display the selected sequence in blue in the map
  - display a thumbnail the hovered sequence
  - hover the sequence selected in the map on the list

- add some test e2e
- maj viewer Geovisio version to 2.2.1
- fix some CSS

## [2.2.2] - 2023-10-16

### Changed

- fix lazy loading on imgs
- fix css to the license link

## [2.2.1] - 2023-10-16

### Changed

- fix some wordings
- licence text in bold
- remove footer when logged
- add tiles custom to the sequence page list

## [2.2.0] - 2023-10-13

### Added

- New UI/UX version :
  - new footer
  - new header
  - add a map to the sequence list page + the uploaded date
  - new wordings

### Changed

- Hide buttons delete and hide sequences images when you are not the owner

## [2.1.3] - 2023-09-25

### Changed

- fix the format date from YY to YYYY in a sequence page

### Added

- Add a first version of a footer
- Add a ay11 page

## [2.1.2] - 2023-09-12

### Changed

- add a fix to center the map with ENV variables

## [2.1.1] - 2023-09-06

### Changed

- GeoVisio web viewer upgraded to 2.1.4, [with alls its changes embedded](https://gitlab.com/geovisio/web-viewer/-/blob/develop/CHANGELOG.md?ref_type=heads#213-2023-08-30).
- Dockerfile creates smaller and faster containers, using pre-built website and Nginx for HTTP serving.
- In the upload input you can now choose between gallery and camera on mobile IOS
- Some CSS fix for the responsive of one Sequence
- Insert the report button in the viewer by passing a params

### Added

- Get the License with the API route
- Add ENV var for maxZoom params of the viewer

## [2.1.0] - 2023-08-29

### Added

- A new page `/envoyer` to upload picture with an interface ([#13](https://gitlab.com/geovisio/website/-/issues/13)) :
  - the user can upload multiples pictures with the interface
  - the pictures are sorted by name
  - the user can see all the pictures uploaded and all the errors

### Changed

- Website releases now follow the synced `MAJOR.MINOR` API version rule, meaning that any version >= 2.1 of the website will be compatible with corresponding [GeoVisio API](https://gitlab.com/geovisio/api) version.

### Fixed

- fix a bug in the header hidden sub menu when authentication is not with keycloak

## [0.1.0] - 2023-07-04

### Added

- A new page `/mes-sequences` to access to a list of sequences for a logged user ([#14](https://gitlab.com/geovisio/website/-/issues/14)) :

  - the user can see all his sequences
  - the user can filter sequences
  - the user can enter to a specific sequence

- A new page `/sequence/:id` to access to a sequence of photos for a logged user ([#14](https://gitlab.com/geovisio/website/-/issues/14)) :
  - the user can see the sequence on the map and move on the map from photos to photos
  - the user can see information about the sequence
  - the user can see all the sequence's photos
  - the user can disable and delete one or many photo(s) of the sequence

### Changed

- Header have now a new entry `Mes photos` when the user is logged to access to the sequence list
- The router guard for logged pages has been changed to not call the api to check the token

[unreleased]: https://gitlab.com/geovisio/website/-/compare/2.4.1...develop
[2.4.1]: https://gitlab.com/geovisio/website/-/compare/2.4.0...2.4.1
[2.4.0]: https://gitlab.com/geovisio/website/-/compare/2.3.1...2.4.0
[2.3.1]: https://gitlab.com/geovisio/website/-/compare/2.3.0...2.3.1
[2.3.0]: https://gitlab.com/geovisio/website/-/compare/2.2.3...2.3.0
[2.2.3]: https://gitlab.com/geovisio/website/-/compare/2.2.2...2.2.3
[2.2.2]: https://gitlab.com/geovisio/website/-/compare/2.2.1...2.2.2
[2.2.1]: https://gitlab.com/geovisio/website/-/compare/2.2.0...2.2.1
[2.2.0]: https://gitlab.com/geovisio/website/-/compare/2.1.3...2.2.0
[2.1.3]: https://gitlab.com/geovisio/website/-/compare/2.1.2...2.1.3
[2.1.2]: https://gitlab.com/geovisio/website/-/compare/2.1.1...2.1.2
[2.1.1]: https://gitlab.com/geovisio/website/-/compare/2.1.0...2.1.1
[2.1.0]: https://gitlab.com/geovisio/website/-/compare/0.1.0...2.1.0
[0.1.0]: https://gitlab.com/geovisio/website/-/commits/0.1.0
