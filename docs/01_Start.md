# GeoVisio Website hands-on guide

![GeoVisio logo](https://gitlab.com/geovisio/api/-/raw/develop/images/logo_full.png)

Welcome to GeoVisio __Website__ documentation ! It will help you through all phases of setup, run and develop on GeoVisio Website.

__Note that__ this only covers the Website / front-end component, if you're looking for docs on another component, you may go to [this page](https://gitlab.com/geovisio) instead.

Also, if at some point you're lost or need help, you can contact us through [issues](https://gitlab.com/geovisio/website/-/issues) or by [email](mailto:panieravide@riseup.net).


## Architecture

The website relies on the following technologies and components:

- Frontend website made in [Vue 3](https://vuejs.org/guide/introduction.html)
- Project use [Vite](https://vitejs.dev/guide/) who offer a fast development server and an optimized compilation for production (like webpack)
- The style is made with CSS/SASS and the [bootstrap library](https://getbootstrap.com/)
- [Typescript](https://www.typescriptlang.org/) used to type
- [Jest](https://jestjs.io/fr/) used for unit testing


## All the docs

You might want to dive into docs :

- [Install and setup](./02_Setup.md)
- [Change the settings](./03_Settings.md)
- [Work on the code](./09_Develop.md)
