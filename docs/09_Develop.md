# Work on the code

## Available commands

Note that all the commands and packages used are available in the `package.json` file.

### Compile and Hot-Reload for Development

Launch your dev server :

```sh
npm run dev
```

or

```sh
yarn dev
```

### Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
npm run test:unit
```

or

```sh
yarn test:unit
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```

or

```sh
yarn lint
```
