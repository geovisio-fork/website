# Setup

GeoVisio website can be installed through classic method, or using Docker.

__Contents__

[[_TOC_]]


## Classic install

### System requirements

**You need to have [Nodejs installed](https://nodejs.org/en/download)**
Node version : >=18.13.0

**You need to have [Npm installed](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)**

You can use npm or [yarn](https://yarnpkg.com/) as package manager

### Install

The website can be installed locally by retrieving this repository and installing dependencies:

```sh
# Retrieve source code
git clone https://gitlab.com/geovisio/website.git
cd website/

# Install dependencies
npm install
```

### Build for production

Before building, you need to define a bit of settings. At least, you have to create a `.env` file and edit its content.

```sh
cp env.example .env
```

More details about settings [can be found in docs here](./03_Settings.md).

Then, building for production can be done with these commands:

```sh
npm run build
PORT=3000 npm run start
```

The website is now available at [localhost:3000](http://localhost:3000).


## Docker setup

The [Docker](https://docs.docker.com/get-docker/) deployment is a really convenient way to have a Geovisio website running in an easy and fast way. Note that this setup documentation only covers __GeoVisio front-end__ (website), if you also need an API running, please refer to [Docker API deployment](https://gitlab.com/geovisio/api/-/blob/develop/docs/14_Running_Docker.md).

You can use the provided __Docker Hub__ `geovisio/website:latest` image directly:

```bash
docker run \
	-e VITE_API_URL="https://your.geovisio.api/" \
	-e VITE_INSTANCE_NAME="My Own GeoVisio" \
	-e VITE_TILES="https://your.geovisio.api/vector/tiles/style.json" \
	-p 3000:3000 \
	--name geovisio-website \
	-d \
	geovisio/website:latest
```

This will run a container bound on [localhost:3000](http://localhost:3000).

You can also build the image from the local source with:

```bash
docker build -t geovisio/website:latest .
```


## Next steps

You can check out [the available settings for your instance](./03_Settings.md).
