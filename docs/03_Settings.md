# Settings

Many things can be customized in your GeoVisio Website.

## Basic settings

Low-level settings can be changed through the `.env` file. An example is given in `env.example` file.

Available parameters are:

- `VITE_API_URL`: the URL to the GeoVisio API (with trailing `/`, example: `https://geovisio.fr/`)
- `VITE_INSTANCE_NAME`: the name of the instance (example: `IGN`)
- `VITE_TILES`: the URL of your tiles : default tiles are the Open Street Map Tiles (example: `https://wxs.ign.fr/essentiels/static/vectorTiles/styles/PLAN.IGN/attenue.json`)
- `VITE_MAX_ZOOM`: the max zoom to use on the map (defaults to 24).
- `VITE_ZOOM`: the zoom to use at the initialization of the map (defaults to 0).
- `VITE_CENTER`: the center position to use at the initialization of the map (defaults to 0).
- `VITE_RASTER_TILE`: the raster tile. Example : `https://maplibre.org/maplibre-style-spec/sources/#raster`.
- Settings for the work environment:
  - `NPM_CONFIG_PRODUCTION`: is it production environment (`true`, `false`)
  - `YARN_PRODUCTION`: same as below, but if you use Yarn instead of NPM
  - `VITE_ENV`: `dev`

More settings are available [in official Vite documentation](https://vitejs.dev/guide/env-and-mode.html#env-files)

Note that you can also change the _Vite_ server configuration in the `vite.config.ts` file. See [Vite Configuration Reference](https://vitejs.dev/config/) if you need.

## Wording customization

GeoVisio website can be customized to have wording reflecting your brand, licence and other elements.

All the wordings of the website are on this [locale file](./src/locales/fr.json). In there, you might want to change:

- The website title (properties `title` and `meta.title`)
- The description (property `meta.description`)
- Links to help pages:
  - `upload.description`
  - `upload.footer_description_terminal`

## Visuals

The following images can be changed to make the website more personal:

- Logo: [`src/assets/images/logo.jpeg`](../src/assets/images/logo.jpeg)
- Favicon: [`static/favicon.ico`](../static/favicon.ico)

## Next steps

You may be interested [in developing on the website](./09_Develop.md).
